package cat.itb.firebasedata;

import androidx.test.espresso.Espresso;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class NavigationTest {

    public String USER_TO_BE_TYPED = "user";
    public String PASS_TO_BE_TYPED = "password1";
    public String EMAIL_TO_BE_TYED = "user@exmail.com";

    @Rule
    public ActivityScenarioRule<FormLoginActivity> activityActivityScenarioRule
            = new ActivityScenarioRule<FormLoginActivity>(FormLoginActivity.class);

    @Test
    public void all_move_register_activity_to_main_activity() {
        login_to_register_activity();
        register_to_main_activity();
    }

    @Test
    public void all_move_login_to_main_to_serie_and_back_to_main(){
        login_to_main_activity();
        main_activity_to_serie_activity();
        serie_activity_to_main_activity();
    }

    @Test
    public void all_move_register_to_main_to_serie_and_back_to_main(){
        login_to_register_activity();
        register_to_main_activity();
        main_activity_to_serie_activity();
        serie_activity_to_main_activity();
    }

    @Test
    public void login_to_main_activity() {
        onView(withId(R.id.textInputEditTextUsername)).perform(typeText(USER_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.textInputEditTextPassword)).perform(typeText(PASS_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.buttonLogin)).perform(click());
        onView(withId(R.id.mainActivityId)).check(matches(isDisplayed()));
    }

    @Test
    public void login_to_register_activity(){
        onView(withId(R.id.buttonToRegister)).perform(click());
        onView(withId(R.id.registerActivityId)).check(matches(isDisplayed()));
    }

    @Test
    public void register_to_login_activity(){
        onView(withId(R.id.buttonToLoginRegAct)).perform(click());
        onView(withId(R.id.loginActivityId)).check(matches(isDisplayed()));
    }

    @Test
    public void move_register_login() {
        login_to_register_activity();
        register_to_login_activity();
    }

    @Test
    public void main_activity_to_serie_activity() {
        onView(withId(R.id.floatingActionButton)).perform(click());
        onView(withId(R.id.seriesActivityId)).check(matches(isDisplayed()));
    }

    @Test
    public void register_to_main_activity() {
        onView(withId(R.id.textInputEditTextUsernameRegAct)).perform(typeText(USER_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.textInputEditTextEmailRegAct)).perform(typeText(EMAIL_TO_BE_TYED)).perform(closeSoftKeyboard());
        onView(withId(R.id.textInputEditTextPasswordRegAct)).perform(typeText(PASS_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.buttonLoginRegAct)).perform(click());
        onView(withId(R.id.mainActivityId)).check(matches(isDisplayed()));
    }

    @Test
    public void serie_activity_to_main_activity() {
        Espresso.pressBack();
        onView(withId(R.id.mainActivityId)).check(matches(isDisplayed()));
    }

}
