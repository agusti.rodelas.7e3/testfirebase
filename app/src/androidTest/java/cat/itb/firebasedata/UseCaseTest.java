package cat.itb.firebasedata;


import android.view.View;
import android.widget.RatingBar;

import androidx.test.espresso.ViewAction;
import androidx.test.espresso.action.GeneralLocation;
import androidx.test.espresso.action.GeneralSwipeAction;
import androidx.test.espresso.action.Press;
import androidx.test.espresso.action.Swipe;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.Intents.intending;
import static androidx.test.espresso.intent.matcher.IntentMatchers.toPackage;
import static androidx.test.espresso.matcher.ViewMatchers.assertThat;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class UseCaseTest {

    public String TEXT_TO_BE_FOUND_ON_TITLE = "Mi vecino Totoro";
    public String TEXT_TO_BE_FOUND_ON_PRODUCER = "Gibli";
    public float RATING = 5f;

    @Rule
    public ActivityScenarioRule<MainActivity> activityActivityScenarioRule
            = new ActivityScenarioRule<MainActivity>(MainActivity.class);

    @Test
    public void insert_serie() {
        onView(withId(R.id.floatingActionButton)).perform(click());
        onView(withId(R.id.seriesActivityId)).check(matches(isDisplayed()));
        onView(withId(R.id.textInputEditTextTitle)).perform(typeText(TEXT_TO_BE_FOUND_ON_TITLE)).perform(closeSoftKeyboard());
        onView(withId(R.id.textInputEditTextTitle)).check(matches(withText(TEXT_TO_BE_FOUND_ON_TITLE)));

        onView(withId(R.id.textInputEditTextProducer)).perform(typeText(TEXT_TO_BE_FOUND_ON_PRODUCER)).perform(closeSoftKeyboard());
        onView(withId(R.id.textInputEditTextProducer)).check(matches(withText(TEXT_TO_BE_FOUND_ON_PRODUCER)));


    }

    @Test
    public void drop_serie() {
    }

    @Test
    public void select_serie() {
        try {
            Thread.sleep(1500);
            onView(withId(R.id.recyclerView))
                    .perform(RecyclerViewActions.actionOnItemAtPosition(3, click()));
            onView(withId(R.id.seriesActivityId)).check(matches(isDisplayed()));
            onView(withId(R.id.textInputEditTextTitle)).check(matches(withText(TEXT_TO_BE_FOUND_ON_TITLE)));
            onView(withId(R.id.textInputEditTextProducer)).check(matches(withText(TEXT_TO_BE_FOUND_ON_PRODUCER)));
            onView(withId(R.id.ratingBar)).check(matches(checkRate(RATING)));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    public static Matcher<View> checkRate(final Float rate) {
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View view) {
                if (!(view instanceof RatingBar)) {
                    return false;
                }

                float rating = ((RatingBar) view).getRating();

                if (rating == 0) {
                    return false;
                }

                return rate == rating;
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }

}
