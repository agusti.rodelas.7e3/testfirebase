package cat.itb.firebasedata;

import android.view.View;
import android.widget.RatingBar;

import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.google.android.material.textfield.TextInputLayout;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class TestRecyclerView {

    public String TEXT_TO_BE_FOUND_ON_TITLE = "The Walking Dead";
    public String TEXT_TO_BE_FOUND_ON_PRODUCER = "Fox";
    public float RATING = 4f;

    @Rule
    public ActivityScenarioRule<MainActivity> activityActivityScenarioRule
            = new ActivityScenarioRule<MainActivity>(MainActivity.class);

    @Test
    public void main_activity_to_serie_activity() {
        try {
            Thread.sleep(1500);
            onView(withId(R.id.recyclerView))
                    .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
            onView(withId(R.id.seriesActivityId)).check(matches(isDisplayed()));
            onView(withId(R.id.textInputEditTextTitle)).check(matches(withText(TEXT_TO_BE_FOUND_ON_TITLE)));
            onView(withId(R.id.textInputEditTextProducer)).check(matches(withText(TEXT_TO_BE_FOUND_ON_PRODUCER)));
            onView(withId(R.id.ratingBar)).check(matches(checkRate(RATING)));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    public static Matcher<View> checkRate(final Float rate) {
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View view) {
                if (!(view instanceof RatingBar)) {
                    return false;
                }

                float rating = ((RatingBar) view).getRating();

                if (rating == 0) {
                    return false;
                }

                return rate == rating;
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }
}
