package cat.itb.firebasedata;

import android.view.View;

import androidx.test.InstrumentationRegistry;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.google.android.material.textfield.TextInputLayout;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class LoginRegisterTest {

    public String USER_TO_BE_TYPED = "user";
    public String PASS_TO_BE_TYPED = "password1";
    public String EMAIL_TO_BE_TYED = "user@exmail.com";

    public String ERROR_EMAIL_TO_BE_TYED = "user";

    @Rule
    public ActivityScenarioRule<FormLoginActivity> activityActivityScenarioRule
            = new ActivityScenarioRule<FormLoginActivity>(FormLoginActivity.class);

    @Test
    public void check_login() {
        check_if_no_text_is_entered();
        onView(withId(R.id.textInputEditTextUsername)).perform(typeText(USER_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.textInputEditTextPassword)).perform(typeText(PASS_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.buttonLogin)).perform(click());
        onView(withId(R.id.mainActivityId)).check(matches(isDisplayed()));

    }

    @Test
    public void check_register() {
        to_register_activity();
        to_login_activity();
        check_error_on_text_input();
        onView(withId(R.id.textInputEditTextUsernameRegAct)).perform(typeText(USER_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.textInputEditTextEmailRegAct)).perform(typeText(EMAIL_TO_BE_TYED)).perform(closeSoftKeyboard());
        onView(withId(R.id.textInputEditTextPasswordRegAct)).perform(typeText(PASS_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.buttonLoginRegAct)).perform(click());
        onView(withId(R.id.mainActivityId)).check(matches(isDisplayed()));

    }

    @Test
    public void check_if_no_text_is_entered() {
        onView(withId(R.id.buttonLogin)).perform(click());
        onView(withId(R.id.inputLayoutUsername)).check(matches(hasTextInputLayoutErrorText("No deixis el camp buit.")));
        onView(withId(R.id.inputLayoutPassword)).check(matches(hasTextInputLayoutErrorText("No deixis el camp buit.")));
    }

    @Test
    public void to_register_activity(){
        onView(withId(R.id.buttonToRegister)).perform(click());
        onView(withId(R.id.registerActivityId)).check(matches(isDisplayed()));
    }

    @Test
    public void to_login_activity(){
        onView(withId(R.id.buttonToLoginRegAct)).perform(click());
        onView(withId(R.id.loginActivityId)).check(matches(isDisplayed()));
    }

    @Test
    public void check_error_on_text_input(){
        to_register_activity();
        onView(withId(R.id.buttonLoginRegAct)).perform(click());
        onView(withId(R.id.inputLayoutUsernameRegAct)).check(matches(hasTextInputLayoutErrorText("No deixis el camp buit.")));
        onView(withId(R.id.inputLayoutEmailRegAct)).check(matches(hasTextInputLayoutErrorText("No deixis el camp buit.")));
        onView(withId(R.id.inputLayoutPasswordRegAct)).check(matches(hasTextInputLayoutErrorText("No deixis el camp buit.")));
        onView(withId(R.id.textInputEditTextEmailRegAct)).perform(typeText(ERROR_EMAIL_TO_BE_TYED)).perform(closeSoftKeyboard());
        onView(withId(R.id.buttonLoginRegAct)).perform(click());
        onView(withId(R.id.inputLayoutEmailRegAct)).check(matches(hasTextInputLayoutErrorText("El format del correo no es correcte.")));
    }

    public static Matcher<View> hasTextInputLayoutErrorText(final String expectedErrorText) {
        return new TypeSafeMatcher<View>() {
            @Override
            public boolean matchesSafely(View view) {
                if (!(view instanceof TextInputLayout)) {
                    return false;
                }

                CharSequence error = ((TextInputLayout) view).getError();

                if (error == null) {
                    return false;
                }

                String errorText = error.toString();

                return expectedErrorText.equals(errorText);
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }
}
