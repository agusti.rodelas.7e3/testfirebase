package cat.itb.firebasedata.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.snackbar.Snackbar;

import cat.itb.firebasedata.R;
import cat.itb.firebasedata.Repository;
import cat.itb.firebasedata.SerieActivity;
import cat.itb.firebasedata.models.Serie;

public class AdapterSerie extends FirebaseRecyclerAdapter<Serie, AdapterSerie.SerieHolder> {

    private FirebaseRecyclerOptions<Serie> options;
    private Context context;
    private Repository rp;

    public AdapterSerie(@NonNull FirebaseRecyclerOptions<Serie> options, Context context, Repository rp) {
        super(options);
        this.context = context;
        this.rp = rp;
    }

    @Override
    protected void onBindViewHolder(@NonNull SerieHolder holder, int position, @NonNull Serie model) {
        holder.bind(model);
    }

    @NonNull
    @Override
    public SerieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        return new SerieHolder(v);
    }

    public void deleteItem(int position) throws ClassNotFoundException {
        String key = getSnapshots().getSnapshot(position).getKey();

        Serie model = (Serie) getSnapshots().getSnapshot(position).getValue(Serie.class);
        rp.deleteSerie(key);
        rp.deleteImage(model.getImageUrl());
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }
    public class SerieHolder extends RecyclerView.ViewHolder {
        TextView textViewNom;
        TextView textViewProductora;
        TextView textViewRate;
        ShapeableImageView imageViewCover;


       public SerieHolder(@NonNull View itemView) {
           super(itemView);
           textViewNom = itemView.findViewById(R.id.textViewNom);
           textViewProductora = itemView.findViewById(R.id.textViewProductora);
           textViewRate = itemView.findViewById(R.id.textViewRate);
           imageViewCover = itemView.findViewById(R.id.imageViewCover);

       }

       public void bind(final Serie model){
           textViewNom.setText(model.getNom());
           textViewProductora.setText(model.getProductora());
           textViewRate.setText(String.valueOf(model.getRate()));
           rp.getImage(model.getImageUrl(), imageViewCover, context);
           itemView.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   Intent i = new Intent(context, SerieActivity.class);
                   i.putExtra("Serie", model);
                   context.startActivity(i);
               }
           });
       }

   }

    public Context getContext() {
        return context;
    }
}
