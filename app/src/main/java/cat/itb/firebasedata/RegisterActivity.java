package cat.itb.firebasedata;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.textfield.TextInputLayout;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {

    TextInputLayout usernameText, passwordText, emailText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        usernameText = findViewById(R.id.inputLayoutUsernameRegAct);
        passwordText = findViewById(R.id.inputLayoutPasswordRegAct);
        emailText = findViewById(R.id.inputLayoutEmailRegAct);

        findViewById(R.id.buttonToLoginRegAct).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent log = new Intent(getApplicationContext(), FormLoginActivity.class);
                startActivity(log);
            }
        });

        findViewById(R.id.buttonLoginRegAct).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean success = true;
                String username = usernameText.getEditText().getText().toString();
                String password = passwordText.getEditText().getText().toString();
                String email = emailText.getEditText().getText().toString();

                if (username.isEmpty()){
                    success = false;
                    usernameText.setError(getString(R.string.error_no_text));
                }else if(usernameText.isErrorEnabled()) {
                    usernameText.setErrorEnabled(false);
                }

                if(password.isEmpty()){
                    success = false;
                    passwordText.setError(getString(R.string.error_no_text));
                }else if(passwordText.isErrorEnabled()) {
                    passwordText.setErrorEnabled(false);
                }

                if(email.isEmpty()){
                    success = false;
                    emailText.setError(getString(R.string.error_no_text));
                }else if(!isEmailValid(email)){
                    success = false;
                    emailText.setError("El format del correo no es correcte.");
                }else if(emailText.isErrorEnabled()) {
                    emailText.setErrorEnabled(false);
                }

                if(success){
                    Intent init = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(init);
                }
            }
        });
    }

    boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}