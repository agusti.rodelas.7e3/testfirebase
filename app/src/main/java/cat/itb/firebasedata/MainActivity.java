package cat.itb.firebasedata;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import cat.itb.firebasedata.adapters.AdapterSerie;
import cat.itb.firebasedata.models.Serie;

public class MainActivity extends AppCompatActivity {

    Repository rp;

    RecyclerView recyclerView;
    AdapterSerie adapter;

    FloatingActionButton button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.floatingActionButton);
        recyclerView = findViewById(R.id.recyclerView);

        rp = new Repository();

        setUpRecyclerView();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), SerieActivity.class);
                startActivity(i);
            }
        });

    }

    private void setUpRecyclerView(){
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        FirebaseRecyclerOptions<Serie> options = new FirebaseRecyclerOptions.Builder<Serie>()
                                                        .setQuery(rp.getMyRef(), Serie.class).build();
        adapter = new AdapterSerie(options, this, rp);
        recyclerView.setAdapter(adapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new SwipeToDeleteCallback(adapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);

    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    public void generarSerie(){
        String[] noms = {"tlou", "The Walking Dead", "Gambito de dama", "Strenger things", "Attack on titan"};
        String[] prod = {"HBO", "Fox", "Netflix", "Netflix", "Mappa"};
        double[] rates = {3, 4.5, 4.5, 3.5, 5};
        for(int i = 0; i < 5; i++){
            String key = rp.getMyRef().push().getKey();
            Serie s = new Serie(key, noms[i], prod[i], rates[i], "");
            s.setIdSerie(key);
            rp.getMyRef().child(key).setValue(s);
        }
    }
}