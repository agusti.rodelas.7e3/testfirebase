package cat.itb.firebasedata;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.google.android.material.textfield.TextInputLayout;

import java.io.InputStream;

import cat.itb.firebasedata.models.Serie;

public class SerieActivity extends AppCompatActivity {

    private static final int PICK_IMAGE = 1;
    TextInputLayout textInputLayouttitol;
    TextInputLayout textInputLayoutproductora;
    RatingBar ratingBar;
    Button buttonAdd;
    Bundle bundle;
    ImageView imageViewCover;
    boolean isChange = false;

    Repository rp;

    Serie se;
    Uri imageUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serie);

        textInputLayouttitol = findViewById(R.id.inputLayoutTitol);
        textInputLayoutproductora = findViewById(R.id.inputLayoutProductora);
        imageViewCover = findViewById(R.id.imageViewPortada);
        ratingBar = findViewById(R.id.ratingBar);
        buttonAdd = findViewById(R.id.buttonAction);

        rp = new Repository();

        bundle = getIntent().getExtras();

        if(bundle != null){
            setOnSerieRecive();
        }

        imageViewCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(gallery, PICK_IMAGE);
            }
        });

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bundle != null){
                    if(checkAllCorrect()){
                        se.setNom(textInputLayouttitol.getEditText().getText().toString());
                        se.setProductora(textInputLayoutproductora.getEditText().getText().toString());
                        se.setRate(ratingBar.getRating());
                        rp.updateSerie(se, new Repository.OnUpload() {
                            @Override
                            public void onSuccess(boolean isUpload) {
                                if(isChange) {
                                    rp.updateImageCover(se.getImageUrl(), imageUri, new Repository.OnUpload() {
                                        @Override
                                        public void onSuccess(boolean isUpload) {
                                            finish();
                                        }
                                    });
                                }
                            }
                        });

                    }

                }else {
                    if(checkAllCorrect()){
                        Serie s = new Serie("s", textInputLayouttitol.getEditText().getText().toString(),
                                textInputLayoutproductora.getEditText().getText().toString(),
                                ratingBar.getRating(),
                                " ");
                        rp.addSerie(s);
                        s.setImageUrl(rp.uploadImageCover(imageUri));
                        rp.updateSerie(s, new Repository.OnUpload() {
                            @Override
                            public void onSuccess(boolean isUpload) {
                                if(isUpload){
                                    finish();
                                }
                            }
                        });


                    }
                }
            }
        });

    }

    public void setOnSerieRecive(){
        se = (Serie) bundle.getSerializable("Serie");
        rp.getImage(se.getImageUrl(), imageViewCover, getApplicationContext());
        textInputLayouttitol.getEditText().setText(se.getNom());
        textInputLayoutproductora.getEditText().setText(se.getProductora());
        ratingBar.setRating((float) se.getRate());
        buttonAdd.setText("Update");
    }

    public boolean checkAllCorrect(){
        if(textInputLayouttitol.getEditText().getText().toString().isEmpty()){
            textInputLayouttitol.setError("El cap no pot estar buit.");
            return false;
        }else if(textInputLayoutproductora.getEditText().getText().toString().isEmpty()){
            textInputLayoutproductora.setError("El cap no pot estar buit.");
            return false;
        }else {
            return true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            imageUri = data.getData();
            imageViewCover.setImageURI(imageUri);
            imageViewCover.setScaleType(ImageView.ScaleType.CENTER_CROP);
            isChange = true;
        }
    }
}