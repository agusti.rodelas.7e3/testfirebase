package cat.itb.firebasedata.models;

import java.io.Serializable;

public class Serie implements Serializable {
    private String idSerie;
    private String nom;
    private String productora;
    private double rate;
    private String imageUrl;

    public Serie() {
    }

    public Serie(String idSerie, String nom, String productora, double rate, String imageUrl) {
        this.idSerie = idSerie;
        this.nom = nom;
        this.productora = productora;
        this.rate = rate;
        this.imageUrl = imageUrl;
    }

    public String getIdSerie() {
        return idSerie;
    }

    public void setIdSerie(String idSerie) {
        this.idSerie = idSerie;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getProductora() {
        return productora;
    }

    public void setProductora(String productora) {
        this.productora = productora;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
