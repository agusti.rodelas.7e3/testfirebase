package cat.itb.firebasedata;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.textfield.TextInputLayout;

public class FormLoginActivity extends AppCompatActivity {

    TextInputLayout usernameText, passwordText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_login);

        usernameText = findViewById(R.id.inputLayoutUsername);
        passwordText = findViewById(R.id.inputLayoutPassword);

        findViewById(R.id.buttonToRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent reg = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(reg);
            }
        });

        findViewById(R.id.buttonLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean success = true;
                String username = usernameText.getEditText().getText().toString();
                String password = passwordText.getEditText().getText().toString();


                if (username.isEmpty()){
                    success = false;
                    usernameText.setError(getString(R.string.error_no_text));
                }else if(usernameText.isErrorEnabled()) {
                    usernameText.setErrorEnabled(false);
                }

                if(password.isEmpty()){
                    success = false;
                    passwordText.setError(getString(R.string.error_no_text));
                }else if(passwordText.isErrorEnabled()) {
                    passwordText.setErrorEnabled(false);
                }

                if(success){
                    Intent init = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(init);
                }

            }
        });
    }
}