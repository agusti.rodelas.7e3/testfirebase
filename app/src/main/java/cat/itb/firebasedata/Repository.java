package cat.itb.firebasedata;

import android.content.Context;
import android.net.Uri;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import cat.itb.firebasedata.adapters.AdapterSerie;
import cat.itb.firebasedata.models.Serie;

public class Repository {

    private FirebaseDatabase db;
    private FirebaseStorage storage;
    private DatabaseReference myRef;
    private StorageReference imRef;

    private boolean uploadComplete = true;

    public Repository() {
        db = FirebaseDatabase.getInstance();
        storage = FirebaseStorage.getInstance();
        myRef = db.getReference("Series");
        imRef = storage.getReference("/Cover");
    }

    public FirebaseDatabase getDb() {
        return db;
    }

    public void setDb(FirebaseDatabase db) {
        this.db = db;
    }

    public DatabaseReference getMyRef() {
        return myRef;
    }

    public void setMyRef(DatabaseReference myRef) {
        this.myRef = myRef;
    }

    public StorageReference getImRef() {
        return imRef;
    }

    public void setImRef(StorageReference imRef) {
        this.imRef = imRef;
    }

    public void addSerie(Serie s){
        String key = myRef.push().getKey();
        s.setIdSerie(key);
        myRef.child(key).setValue(s);
    }

    public void updateSerie(Serie s, final OnUpload listener){
        myRef.child(s.getIdSerie()).setValue(s).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    listener.onSuccess(true);
                }
            }
        });
    }

    public void deleteSerie(String key){
        myRef.child(key).removeValue();
    }

    public String uploadImageCover(Uri uriImage){
        String imageReference = "";
        UploadTask uploadTask = imRef.child(uriImage.getLastPathSegment()).putFile(uriImage);
        imageReference = uriImage.getLastPathSegment();
        return imageReference;
    }

    public interface OnUpload{
        public void onSuccess(boolean isUpload);
    }

    public void updateImageCover(String name, Uri uri, final OnUpload listener){
        StorageTask uploadTask = imRef.child(name).putFile(uri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if(task.isSuccessful()){
                    listener.onSuccess(true);
                }
            }
        });
    }

    /**
     * Carga la imagen en la view seleccionada según carpeta contenedora i nombre
     *
     * @param name nombre 
     * @param v
     * @param context
     */
    public void getImage(String name, final View v, final Context context){
        imRef.child(name).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                RequestOptions options = new RequestOptions();
                options.centerCrop();
                Log.d("url", uri.toString());
                Glide.with(context)
                        .load(uri)
                        .apply(options)
                        .into((ImageView) v);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d("fallo url", exception.toString());
            }
        });
    }

    public void deleteImage(String name){
        imRef.child(name).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d("TAG Succes", "onSuccess: deleted file");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d("TAG failure", "onFailure: did not delete file");
            }
        });
    }


}
